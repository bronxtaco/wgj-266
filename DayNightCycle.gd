extends Node2D

onready var g = get_node("/root/Global")
onready var animationPlayer = $AnimationPlayer

func _ready():
	PlayNight()

func PlayNight():
	animationPlayer.play("Night")
	
func PlayDay():
	animationPlayer.play("Day")

func SetDayActive(_active):
	g.Daytime = _active
