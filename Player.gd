extends KinematicBody2D

onready var g = get_node("/root/Global")
onready var animatedSprite = $AnimatedSprite
onready var deathTimer = $DeathTimer

var GRAV = 18
var RUN_SPEED = 240
var SHIELD_SPEED = 90
var JUMP_FORCE = -500
var BURN_RATE = 0.005

enum State {
	GROUND,
	AIR,
	SHIELD,
	DEAD,
}

var m_state = State.AIR
var m_velocity = Vector2(0, 0)
var m_burn = 1.0

func _ready():
	Events.connect("hit_player", self, "_on_hit_player")
	animatedSprite.play("idle")
	
func _physics_process(_delta):
	if m_state == State.DEAD:
		var totalTime = deathTimer.wait_time
		var currentTime = deathTimer.time_left
		animatedSprite.modulate.a = currentTime / totalTime
		return
	
	var move = 0
	if Input.is_action_pressed("moveRight"):
		move += 1
	if Input.is_action_pressed("moveLeft"):
		move -= 1
	
	if m_state == State.SHIELD:
		if !Input.is_action_pressed("shield"):
			m_state = State.GROUND
		else:
			if move > 0:
				m_velocity.x = SHIELD_SPEED
				animatedSprite.flip_h = false
				animatedSprite.play("shieldWalk")
			elif move < 0:
				m_velocity.x = -SHIELD_SPEED
				animatedSprite.flip_h = true
				animatedSprite.play("shieldWalk")
			else:
				m_velocity.x = 0
				animatedSprite.play("shieldIdle")
	
	if m_state != State.SHIELD:	
		#air and ground
		if move > 0:
			m_velocity.x = RUN_SPEED
			animatedSprite.flip_h = false
		elif move < 0:
			m_velocity.x = -RUN_SPEED
			animatedSprite.flip_h = true
		else:
			m_velocity.x = 0
		
		if m_state == State.GROUND:
			if Input.is_action_pressed("shield"):
				m_state = State.SHIELD
			else:
				if move != 0:
					animatedSprite.play("run")
				else:
					animatedSprite.play("idle")
					
				if Input.is_action_just_pressed("jump"):
					m_velocity.y = JUMP_FORCE
					m_state = State.AIR
					animatedSprite.play("jump")
		elif m_state == State.AIR:
			var col1 = move_and_collide(Vector2.RIGHT, true, true, true)
			var col2 = move_and_collide(Vector2.LEFT, true, true, true)
			if col1 != null or col2 != null:
				m_velocity.x = 0
		
		if g.Daytime:
			m_burn -= BURN_RATE
			if m_burn <= 0:
				m_state = State.DEAD
				animatedSprite.stop()
				deathTimer.start()
		else:
			m_burn += BURN_RATE
			m_burn = min(m_burn, 1)
		animatedSprite.UpdateBurn(m_burn)
		
	var col = move_and_collide(Vector2.DOWN, true, true, true)
	if col == null:
		m_velocity.y += GRAV
		if m_state != State.SHIELD:
			m_state = State.AIR
	
	col = move_and_collide(m_velocity * _delta)
	if col != null:
		var ang = floor(rad2deg(col.get_angle()))
		if ang == 0:
			m_state = State.GROUND
			m_velocity.y = 0
		else:
			m_velocity.x = 0

func _on_DeathTimer_timeout():
	pass # end game

func _on_hit_player():
	if m_state == State.DEAD:
		return
	m_state = State.DEAD
	animatedSprite.play("dead")
	deathTimer.start()
