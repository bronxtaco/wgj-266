extends Node2D

onready var enemyPath1 = $EnemyPath1
onready var enemyPath2 = $EnemyPath2

func _ready():
	enemyPath1.get_node("AnimationPlayer").play("EnemyPath")
	enemyPath2.get_node("AnimationPlayer").play("EnemyPath")
